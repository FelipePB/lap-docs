# Formato de documentación - LAP

Estandarizar la forma en que se escribe la documentación de los proyectos.

*by [Felipe Piña Bandera](https://gitlab.com/FelipePB) and [Laboratorio de Analítica Pública](https://gitlab.com/lecegit).*

---
## Tutoriales 

Estos son los tutoriales disponibles para el equipo LAP:

* [**Changelog Tutorial**](tutorial/changelog-tutorial.md)
* [**Readme Tutorial**](tutorial/readme-tutorial.md)
* [**Git Commit Message Tutorial**](tutorial/git-commit-message-tutorial.md)
* [**Git Branching Model Tutorial**](tutorial/git-branching-model-tutorial.md)

## Mejoras

Puedes dejar tus mejoras como *issues* y hacer los *pull requests* que consideres necesarios para consolidar la documentación.
