# Changelog Tutorial - LAP
Estandarizar la forma en que se documentan los CHANGELOGs en el LAP.

*by [Felipe Piña Bandera](https://gitlab.com/FelipePB) and [Laboratorio de Analítica Pública](https://gitlab.com/lecegit).*

## Tabla de contenidos
* [Introducción](#introducción)
* [Hacer un buen changelog](#hacer-un-buen-changelog)
	* [Directrices](#directrices)
 	* [Tipos de cambios](#tipos-de-cambios)
* [Minimizar el mantenimiento del changelog](#minimizar-el-mantenimiento-del-changelog)
* [Formato general](#formato-general)
	* [*Versionado semántico*](#versionado-semántico)
	* [Etiquetas (*tags*)](#etiquetas-tags)
* [Ejemplo de changelog usando *versionamiento semántico*](#ejemplo-de-changelog-usando-versionamiento-semántico)
* [Conclusión](#conclusión)

## Introducción

Un ChangeLog contiene una lista cronológicamente ordenada de los cambios más destacados para cada versión del proyecto. Facilita a colaboradores y usuarios saber las diferencias entre ellas. 

Para dudas y otros revisa [Keep a ChangeLog](https://keepachangelog.com/en/1.0.0/), que es la página guía en que está basado este resumen.

## Hacer un buen changelog

### Directrices

-   Están hechos *para los seres humanos*, no para las máquinas.
-   Debe haber una entrada para cada versión.
-   Los mismos tipos de cambios deben ser agrupados.
-   Versiones y secciones deben ser enlazables.
-   La última versión va primero.
-   Debe mostrar la fecha de publicación de cada versión.
-   Indicar si el proyecto sigue el  [*Versionamiento Semántico*](https://semver.org/). Recuerda que en LAP sí se sigue el formato mencionado.

### Tipos de cambios

-   `Added`  para funcionalidades nuevas.
-   `Changed`  para los cambios en las funcionalidades existentes.
-   `Deprecated`  para indicar que una característica o funcionalidad está obsoleta y que se eliminará en las próximas versiones.
-   `Removed`  para las características en desuso que se eliminaron en esta versión.
-   `Fixed`  para corrección de errores.
-   `Security`  en caso de vulnerabilidades.


## Minimizar el mantenimiento del changelog

Se mentiene una sección con el nombre  `In develop` al principio del cuerpo del archivo. Sirve para hacer un seguimiento sobre los próximos cambios y ahorrar trabajo futuro en la siguiente versión del changelog.


## Formato general

 - Se escribirán en inglés. 
 - La parte superior del archivo contendrá un encabezado en donde se especifica que el documento sigue el protocolo del versionamiento semántico. Esto nos permite comparar versiones del código, por lo que en debise representarse de 

```markdown
## [X'.Y'.Z'](https://gitlab.com/JsWorkspace/geoviz-indicators/compare/vX.Y.Z...vX'.Y'.Z')(2019-01-01)

### Add / Fix / Change / Deprecated / Remove / Security
	- ...

## [X.Y.Z](...)(2018-09-18)
```
donde `vX.Y.Z` es la versión a la que apunta la comparación (*target*) y `vX'.Y'.Z'` es la versión con la que se quiere comparar (*source*). 

Dado que la implementación de la nueva documentación es nueva, se considerará que **si el tipo de arreglo implica que cambie la numeración del versionado semántico propuesto, será descrito en el archivo** (con tal de poder hacer la comparación).


### *Versionado semántico*

Como se puede ver en el nombre de la ramas *release* y *hotfix* aparece el sufijo `vX.Y.Z` para indicar el versionamiento del código, algo muy frecuente en los proyectos que van creciendo en iteraciones en su desarrollo. [Tom Preston-Werner](http://tom.preston-werner.com/), co-fundador de GitHub, propuso la [Semantic Versioning](https://semver.org/), muy popular a nivel internacional y recomendada por [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) (de donde nos basamos para el tutorial de *changelog*). En ella se distinguen tres números para el versionamiento: cambios mayores, cambios menores y parches.

Citando al [sitio en español](https://semver.org/lang/es/) del *versionado semántico*:

>Considera un formato de versión del tipo X.Y.Z (Major.Minor.Patch) Los arreglos de bugs que no cambian el API incrementan el patch, los cambios y adiciones que no rompen la compatibilidad de las dependencias anteriores incrementan el minor, y los cambios que rompen la compatibilidad incrementan el major.

Para más detalles sobre qué considerar como cambio mayor, menor, parche u otro visita el [tutorial hecho para *commits* en Git](#git-commit-message-tutorial). En él se especifican la mayoría de los casos que pueden ocurrir.

>**Nota:** una versión previa a la de lanzamiento (*pre-release*) puede estar representada por los números previamente vistos, acompañados de un guión y de caracteres ASCII alfanuméricos. La precedencia debe ser calculada separando la versión en major, minor, patch e identificadores pre-release en ese orden. Los identificadores numéricos siempre tienen una precedencia menor que los no-numéricos. 
>
>*Ejemplo: 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0.*

Si bien lo anterior es rígido, puede amoldarse a lo que ocurra en los diferentes proyectos. Dado que son las primeras iteraciones de la documentación, se espera conseguir un conjunto de ideas/recomendaciones de todos los que colaboran.

### Etiquetas (*tags*)

Las etiquetas nos permiten catalogar diferentes *commits* y así poder comparar diferentes versiones del proyecto.

Tal como se menciona en la guía de modelado de ramas, [Git Branching Model](https://nvie.com/posts/a-successful-git-branching-model/#finishing-a-release-branch), las etiquetas se deben hacer en `master`, es decir, después de hacer el *merge* desde `release` hacia `master` y previo al *push* de la siguiente forma:
```bash
git tag -a vX.Y.Z
```
donde `X`,`Y` y `Z` son números que corresponden al [*versionado semántico*](#versionado-semántico) establecido.

Cuando se añade una etiqueta a un *commit*, solo permanece disponible a nivel local. Para dejarlas disponibles de manera remota es necesario hacer un *push* referenciándola:
 
```bash
$ git tag                         # ver lista de etiquetas
$ git push origin <tag-name>      # push una sola etiqueta
```

>**Nota**: si se han hecho varios *commits* antes de un *push*, la etiqueta se asocia al último de ellos por defecto, a menos que se especifique uno específico.

## Ejemplo de changelog

A continuación se muestra un ejemplo de changelog basado en lo expuesto por [Keep a ChangeLog](https://keepachangelog.com/en/1.0.0/) que utiliza el versionamiento semántico expuesto, las etiquetas y :
```markdown 
# ChangeLog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) since version 3.0.0 and [Conventional Commits](https://www.conventionalcommits.org/).

## [In develop]

### To add
	- API implemention for all required data.
	- New indicator 'ruralidad'.


## [3.0.2](https://gitlab.com/JsWorkspace/geoviz-indicators/compare/v3.0.1...v3.0.2) (2019-01-18)

### Fix
	- Alert message from cases without data.
	- ...


## [3.0.1](https://gitlab.com/JsWorkspace/geoviz-indicators/compare/v3.0.0...v3.0.1) (2019-01-17)

### Change
	- ...


## [3.0.0](https://gitlab.com/JsWorkspace/geoviz-indicators/compare/v2.0.0...v3.0.0) (2019-01-16)

### Add
	- Custom leaflet control message while is loading/rendering district block polygon, showing actual geojson render.
	- ...

### Change
	- Polygon load and render: using only districts data to show any territorial unity (district, SLE or city).
	- ...

### Remove
	- Clean code: useless comments and methods.
	- ...

### Fix
	- Bashes to run app in a *web server*.
	- ...


## [2.0.0](https://gitlab.com/JsWorkspace/geoviz-indicators/compare/v2.0.0...v3.0.0) (2018/03/23)
	- Added block map UI.
	- ...

```

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0MjQzMzYyMzcsLTE5NDY4NTQ3NThdfQ
==
-->