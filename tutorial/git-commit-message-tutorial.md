# Git Commit Message Tutorial - LAP
 
Guía para formular un buen commit basado en una serie de artículos.

 *by [Felipe Piña Bandera](https://gitlab.com/FelipePB) and [Laboratorio de Analítica Pública](https://gitlab.com/lecegit).*

## Tabla de contenidos

* [Formato de un buen *commit*](#formato-de-un-buen-commit)
	* [*How to Write a Git Commit Message*](#formato-de-un-buen-commit)
	* [*Conventional Commits*](#conventional-commits)
		* [Tipo](#tipo)
		* [*Scope*](#scope)
	* [Lo mejor de ambas: *Template* para *commits*](#lo-mejor-de-ambas-template-para-commits)

## Formato de un buen *commit*

Otra cosa a estandarizar es el nombramiento de los *commits*. Suele suceder que hay diferencias notorias entre los nombres que los commits, donde unos proporcionan mayor información y otros no exponen algo útil cuando actualizan sus cambios.

Para eso nos basamos en dos páginas, el primero es el artículo [*How to Write a Git Commit Message*](https://chris.beams.io/posts/git-commit/), y el segundo una página dedicada a la convención de commits, .

### *How to Write a Git Commit Message*

En [este artículo](https://chris.beams.io/posts/git-commit/) se expone el problema anteriormente descrito y se presentan [siete reglas para hacer un buen *commit*](https://chris.beams.io/posts/git-commit/#seven-rules), enfocándose en su título y cuerpo:

1.  [Separar el título del cuerpo por una línea blanca](https://chris.beams.io/posts/git-commit/#separate)
2.  [Delimitar el largo del título a 50 caracteres](https://chris.beams.io/posts/git-commit/#limit-50)
3.  [Empezar con mayúscula el título](https://chris.beams.io/posts/git-commit/#capitalize)
4.  [No usar punto final en el título](https://chris.beams.io/posts/git-commit/#end)
5.  [Usar el modo imperativo en el título](https://chris.beams.io/posts/git-commit/#imperative)
6.  [Delimitar el cuerpo a 72 caracteres](https://chris.beams.io/posts/git-commit/#wrap-72)
7.  [Usar el cuerpo para explicar *qué* y *por qué* vs. *cómo* ](https://chris.beams.io/posts/git-commit/#why-not-how)

Un gran ejemplo práctico de un *commit* es expuesto en el [core de Bitcoin](https://github.com/bitcoin/bitcoin/commit/eb0b56b19017ab5c16c745e6da39c53126924ed6), exponiendo de gran forma lo que se cambia y porqué se hace.

### *Conventional Commits*

Sumado a lo anterior, [utilizamos una convención para el uso del formato anterior](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) que determina el tipo de *commit* y cómo se estructura, principalmente, su encabezado (tipo y *scope*).

#### Tipo

Se fijan varios prefijos principales para el prefijo de dicho título, con un par de adaptaciones:

-	`fix`: un *commit* que arregle parte del código base anterior (que corresponde a *Patch* en la nomenclatura del versionamiento semántico) tendrá el prefijo `fix:` en su título, con un cuerpo opcional en caso de ser necesario.
-	`feat`: un *commit* que introduzca una nueva funcionalidad (que corresponde a *Minor* en la nomenclatura del versionamiento semántico) tendrá el prefijo `feat:` en su título, con un cuerpo opcional en caso de ser necesario.
-	Otros: para un *commit* que no sea `fix`, ni `feat` se usa lo propuesto en el repo [commitlint-config-conventional](https://github.com/marionebl/commitlint/tree/master/%40commitlint/config-conventional):
	-   *docs* (cambios en la documentación)
	-   *style* (cambios que no afectan el significado del código: formato, puntos y comas pendientes, etc)
	-   *refactor* (cambio/mejora del código, sin añadir una nueva característica o arreglar un *bug*) 
	-   *test* (agregar tests faltantes, arreglo de tests; no hay cambio en la base del código)
	-   *chore* (cambios en cosas que no verá un usuario final del proyecto: configuración interna (`.gitignore`  o  `.gitattributes`), adición o modificación de librerías, modificación de métodos internos privados, etc; no hay cambio en la base del código). Suelen ser cambios a cosas que en realidad no entran en producción.
- 	`BREAKING CHANGE`: un *commit* con el prefijo `fix:` o `feat:` que contenga `BREAKING CHANGE:` al principio de su cuerpo o de su *footer* implica que hay un cambio mayor en la funcionalidad del producto o de la API (que corresponde a *Mayor* en la nomenclatura del versionamiento semántico)

#### *Scope*

El *scope* (ámbito) podría ser cualquier cosa que especifique el lugar del cambio del *commit*, como `react`, `leaflet`, `webpack`,  `babel`,  `redux`, etc.

### Lo mejor de ambas: *Template* para *commits*

Para simplificar este trabajo basado en lo revisado anteriormente, [usamos un *template* que facilita el relleno del commit](https://codeinthehole.com/tips/a-useful-template-for-commit-messages/) a través de varias preguntas claves:

```text
# <type>(<optional scope>): <subject (If applied, this commit will...)>
# Tipo del commit(ámbito opcional) + título en una línea que continue lo anterior, en modo imperativo, tiempo presente.
# Ej 1: feat: update getting started documentation.
# Ej 2: fix: alert message in cases without polygon data.

# <body (Why is this change needed?)> [recommended]
# Explicar por qué es importante hacerlo.
# Ej 1: Keep updated documentation is very important because...
# Ej 2: BREAKING CHANGE: The alert message in those cases without data has a non-existent email as a customer support and...

# <body (How does it address the issue?)> [recommended]
# Cómo se lleva a cabo el cambio? Técnicamente hablando si es necesario.
# Ej1: Add first lap-docs files version.
# Ej2: Remove the email, changed it for a standard phrase for contact to customer support in method 'getData'...

# <footer (Provide links to any relevant tickets, articles or other resources)> [optional]
# Acá escribir si es que se relaciona con algún issue de git, algún artículo que haya facilitado la solución del mismo u otro enlace de importancia.
# Ej 1: Fix issue #123 (poner enlace a issue si se quiere); 
# Ej 2: This link in stackoverflow was useful: www.stackoverflow.com/.....
```

Para agregar el template a tu proyecto, grábalo en un archivo en cualquier ruta (ej: `~/.git_commit_msg.txt`):

```text
# <type>(<optional scope>): <subject (If applied, this commit will...)>

# <body (Why is this change needed?)> [recommended]

# <body (How does it address the issue?)> [recommended]

# <footer (Provide links to any relevant tickets, articles or other resources)> [optional]

```

luego ejecuta

```bash
$ git config commit.template ~/.git_commit_msg.txt
```

Cada vez que requieras hacer un *commit* importante, ejecutas `git commit` y aparecerá el *template* por completar. Si tu *commit* se cataloga como uno de categoría menor (*Otros* de la sección anterior o similar) y por ello tiene un comentario de una línea, puedes usar `git commit -m "<type>(<optional scope>): <subject (If applied, this commit will...)>"` (ej. `git commit -m "docs: update README missing links"` ).


