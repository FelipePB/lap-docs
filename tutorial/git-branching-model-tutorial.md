# Git Branching Model Tutorial - LAP

Estandarizar la forma en que se realiza el manejo de ramas y *commits* de Git en el LAP.

 *by [Felipe Piña Bandera](https://gitlab.com/FelipePB) and [Laboratorio de Analítica Pública](https://gitlab.com/lecegit).*

## Tabla de contenidos
* [Introducción](#introducción)
* [Desarrollo](#desarrollo)
	* [Ramas principales](#ramas-principales)
 	* [Ramas auxiliares](#ramas-auxiliares)
	 	* [Ramas *feature*](#ramas-feature)
		* [Ramas *release*](#ramas-release)
		* [Ramas *hotfix*](#ramas-hotfix)
	* [*Versionado semántico*](#versionado-semántico)
	* [Etiquetas (*tags*)](#etiquetas-tags)
* [Conclusión](#conclusión)

## Introducción

El repositorio de la visualización de indicadores, [geoviz-indicators](https://gitlab.com/JsWorkspace/geoviz-indicators), es el primero en el que se implementa el nuevo modelo de manejo de ramas. Está basado en [el artículo *'A successful Git branching model'*](https://nvie.com/posts/a-successful-git-branching-model/).

## Desarrollo

A pesar de que se aconseja encarecidamente la lectura constante de las secciones del artículo, tal como lo hizo el autor de este texto, se expone parte de lo más importante.

### Ramas principales
Debe haber dos ramas principales en todo proyecto:

* master
* develop

Se considera que la rama *master* (remotamente llamada *origin/master*) tendrá siempre la versión de código al nivel de producción, lista para ser utilizada en cualquier momento. En cambio, la rama *develop* será la rama principal de desarrollo, desde ella se generan todas las nuevas funcionalidades. Estas ramas van a ser siempre dos paralelas, con el mismo contenido en diferentes instantes de tiempo, tal como se explica.

>**Nota:** Si *develop* no existe, algo que solo ocurre al empezar a estructurar el código de esta forma, se crea desde *master* previo a cualquier desarrollo. 

Cuando lo desarrollado en develop está en un punto estable y listo, los cambios deben devolverse a master, etiquetando los cambios de alguna forma.

### Ramas auxiliares
A pesar de que hay diferentes formas de establecer este modelo de ramas, la guía anterior establece tres tipos de ramas auxiliares para trabajar:

* Ramas *feature* (características)
* Ramas *release* (lanzamientos)
* Ramas *hotfix* (arreglos urgentes)

#### Ramas *feature*

* Se crean desde la rama: `develop`.
* Cambios hechos deben devolverse a la rama: `develop`.
* Convención de nombres para la rama: cualquier cosa sueñe ser válida excepto `master`,  `develop`,  `release-*`, or  `hotfix-*`, para evitar confusiones. Para estandarizarlo, en el LAP se usa `feature/nombre_arreglo_o_funcionalidad`.

Son aquellas que se utilizan para desarrollar cualquier funcionalidad y/o arreglo de una futura versión de producción. La gracia es que la *feature* específica sólo persista hasta que se termina de desarrollar una pequeña funcionalidad, aceptándola (integrándola a develop) o descartándola (eliminándola). 

>**Nota del autor**: evaluar si corresponde agregar una *branch* solo para arreglos.

##### Crear una rama *feature* (siempre desde `develop`)
```bash
$ git checkout -b feature/myFeature develop
# Switched to a new branch "feature/myFeature"
```
##### Agregando una *feature* terminada a develop
```bash
$ git checkout develop
# Switched to branch 'develop'
$ git merge --no-ff features/myFeature
# Updating ea1b82a..05e9557
# (Summary of changes)
$ git branch -d features/myFeature
# Deleted branch features/myFeature (was 05e9557).
$ git push origin develop
```
La opción --no-ff se usa para que siempre se genere un commit, aún cuando se pueda hacer con un `fast-forward`, por varias cosas. Entre ellas, facilitar la visualización de commits y la reversión de un conjunto de ellos, en caso de ser necesario.

#### Ramas *release*
-   Se crean desde la rama: `develop`.
-   Cambios hechos deben devolverse a la rama: `develop` y `master`.
-   Convención de nombre para la rama: `release-vX.Y.Z`. 

Las ramas `release` son la última etapa previo al lanzamiento de una nueva versión de producción. Es posible hacer cambios muy pequeños, sobretodo prepara la información para la versión de producción, así como arreglar algunos errores de funcionalidades presentes.

El momento clave para cambiarse de develop a release es cuando la primera refleja el estado deseado de la nueva versión de producción. Toda funcionalidad que quiera ser agregada posterior a este punto, será añadida a la lista de tareas/funcionalidades por hacer y agregada en una futura versión de producción.

El sufijo `vX.Y.Z` presente en el nombre del *release* será visto más adelante, en la sección del [*versionado semántico*](#versionado-semántico). 

##### Crear una rama *release* (siempre desde *develop*)
```bash
$ git checkout -b release-v1.2 develop
# Switched to a new branch "release-v1.2"
```

##### Agregando una *release* terminada a *master*
```bash
$ git checkout master
# Switched to branch 'master'
$ git merge --no-ff release-v1.2
# Merge made by recursive.
# (Summary of changes)
$ git tag -a v1.2
$ git push origin master
```

##### Agregar cambios menores hechos en *release* a *develop*
```bash
$ git checkout develop
# Switched to branch 'develop'
$ git merge --no-ff release-v1.2
# Merge made by recursive.
# (Summary of changes)
```
---
A esta altura, `release` ya se integró a `master` (y a `develop` si es que hubo cambios). Por lo que se puede eliminar sin problemas:

```bash
$ git branch -d release-v1.2
# Deleted branch release-v1.2 (was ff452fe).
```

#### Ramas *hotfix*
-   Se crean desde la rama: `master`.
-   Cambios hechos deben devolverse a la rama: `develop` y `master`.
-   Convención de nombre para la rama: `hotfix-*`. 

Las ramas `hotfix` se generan para actuar de forma rápida ante situaciones importante y no planificadas, como un *bug* crítico, desde una versión de producción, por ello se crean desde `master`. 

La principal gracia de esto es que no se depende de `develop` y todo lo que esto conlleva. Se debe considerar como una medida para salir del apuro.

##### Crear una rama *hotfix* (siempre desde *master*)
Suponiendo una versión de producción 1.2 (v1.2) con un *bug* importante, se crea *hotfix* como parche para esa versión:

```bash
$ git checkout -b hotfix-v1.2.1 master
# Switched to a new branch "hotfix-v1.2.1"
```
>**Nota:** chequear siempre el número de versión previo a salir/mergear/eliminar del *hotfix*.

##### Agregando una *hotfix* por un problema crítico en *master*
```bash
$ git checkout master
# Switched to branch 'master'
$ git merge --no-ff hotfix-v1.2.1
# Merge made by recursive.
(Summary of changes)
$ git tag -a v1.2.1
```

Incluir arreglo en *develop*, si es que no hay un *release* a punto de salir:
```bash
$ git checkout develop
# Switched to branch 'develop'
$ git merge --no-ff hotfix-v1.2.1
# Merge made by recursive.
# (Summary of changes)
```

Citando literalmente al texto: 
>La única excepción a esta regla es que, cuando haya una rama `release` existente, esperando por pasar a producción, el arreglo hecho en `hotfix` tiene que ser mergeado **en release en vez de develop**. Esto porque `release` sí o sí tiene que pasar a `develop`, en caso de haber cambios (el hotfix es uno), cuando se determine que está listo.
>
>Si el trabajo actual en `develop` requiere el arreglo del *bug* de forma urgente y no puede esperar a que se complete el `release`, entonces se *mergea* directamente a `develop`.

Tras finalizar y haber *mergeado*, se puede eliminar el *branch* creado para sol:
```bash
$ git branch -d hotfix-1.2.1
# Deleted branch hotfix-1.2.1 (was abbe5d6).
```

### *Versionado semántico*

Como se puede ver en el nombre de la ramas *release* y *hotfix* aparece el sufijo `vX.Y.Z` para indicar el versionamiento del código, algo muy frecuente en los proyectos que van creciendo en iteraciones en su desarrollo. [Tom Preston-Werner](http://tom.preston-werner.com/), co-fundador de GitHub, propuso la [Semantic Versioning](https://semver.org/), muy popular a nivel internacional y recomendada por [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) (de donde nos basamos para el tutorial de *changelog*). En ella se distinguen tres números para el versionamiento: cambios mayores, cambios menores y parches.

Citando al [sitio en español](https://semver.org/lang/es/) del *versionado semántico*:

>Considera un formato de versión del tipo X.Y.Z (Major.Minor.Patch) Los arreglos de bugs que no cambian el API incrementan el patch, los cambios y adiciones que no rompen la compatibilidad de las dependencias anteriores incrementan el minor, y los cambios que rompen la compatibilidad incrementan el major.

Para más detalles sobre qué considerar como cambio mayor, menor, parche u otro visita el [tutorial hecho para *commits* en Git](#git-commit-message-tutorial). En él se especifican la mayoría de los casos que pueden ocurrir.

>**Nota:** una versión previa a la de lanzamiento (*pre-release*) puede estar representada por los números previamente vistos, acompañados de un guión y de caracteres ASCII alfanuméricos. La precedencia debe ser calculada separando la versión en major, minor, patch e identificadores pre-release en ese orden. Los identificadores numéricos siempre tienen una precedencia menor que los no-numéricos. 
>
>*Ejemplo: 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0.*

Si bien lo anterior es rígido, puede amoldarse a lo que ocurra en los diferentes proyectos. Dado que son las primeras iteraciones de la documentación, se espera conseguir un conjunto de ideas/recomendaciones de todos los que colaboran.

### Etiquetas (*tags*)

Las etiquetas nos permiten catalogar diferentes *commits* y así poder comparar diferentes versiones del proyecto.

Tal como se menciona en la guía de modelado de ramas, [Git Branching Model](https://nvie.com/posts/a-successful-git-branching-model/#finishing-a-release-branch), las etiquetas se deben hacer en `master`, es decir, después de hacer el *merge* desde `release` hacia `master` y previo al *push* de la siguiente forma:
```bash
git tag -a vX.Y.Z
```
donde `X`,`Y` y `Z` son números que corresponden al [*versionado semántico*](#versionado-semántico) establecido.

Cuando se añade una etiqueta a un *commit*, solo permanece disponible a nivel local. Para dejarlas disponibles de manera remota es necesario hacer un *push* referenciándola:
 
```bash
$ git tag                         # ver lista de etiquetas
$ git push origin <tag-name>      # push una sola etiqueta
```

>**Nota**: si se han hecho varios *commits* antes de un *push*, la etiqueta se asocia al último de ellos por defecto, a menos que se especifique uno específico.


## Conclusión

Acostumbrarse a la rutina de la buena pŕactica en el manejo de ramas puede ser difícil y hasta incómodo en un principio, pero hay que verlo como una prevención: cada proyecto parte de algo muy chico que se va agrandando, en cantidad de código y de personas que trabajan en él. Es importante que el equipo de trabajo considere aplicarlo desde un principio para que futuros colaboradores lo dominen.

Es posible encontrar en la *web* muchas metodologías de manejo de ramas. Esta es solo una de ellas, por lo que el *feedback* es fundamental para consolidar las metodologías de trabajo a seguir.

