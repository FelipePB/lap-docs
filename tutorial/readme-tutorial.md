# README Tutorial - LAP

Estandarizar la forma en que se documentan los READMEs en el LAP.
 
![head_readme](https://i.imgur.com/Aot5coj.gif) 

 *by [Felipe Piña Bandera](https://gitlab.com/FelipePB) and [Laboratorio de Analítica Pública](https://gitlab.com/lecegit).*

## Introducción

El repositorio de la visualización de indicadores, [geoviz-indicators](https://gitlab.com/JsWorkspace/geoviz-indicators), es el primero en el que se implementa la nueva estructura. Está basado en [el repositorio del paquete *vmd*](https://github.com/yoshuawuyts/vmd), uno de los ejemplos expuestos por [Art of Readme](https://github.com/noffle/art-of-readme) (revisado diagonalmente) como recomendación.

## Resumen
Considerando lo visto, se ha establecido que las seccione se separen en lo siguiente:

### Encabezado
* Título identificador.
* Medallas git (pendiente, no sé si necesario).
* Descripción en una línea.
* Enlaces directos a los encabezados del cuerpo. Si se quiere, también es posible de establecer una tabla de contenidos que contenga los hipervínculos hacia partes específicas del texto (preferible en READMEs largos).
* **Opcional:** Incluir enlace a traducción del README en otros idiomas.
* **Recomendado:** Imagen/GIF que muestre la funcionalidad.
  
### Cuerpo
*	Resumen: breve para contextualizar al lector.
*	Requerimientos: de
*	Instalación: detalles del *setup* necesario para la ejecución. 
*	Actualización: detalles para actualizar las versiones 
*	Uso/ejecución: ejemplos de uso de la librería/API.
*	Autor/es.
*	Licencia: MIT por defecto.

## Lista de verificación para un README

A continuación, una lista de comprobación para medir la calidad del fichero
README, expuesta como *bonus* en [una sección de la documentación principal (Art of Readme)](https://github.com/noffle/art-of-readme#bonus-the-readme-checklist):

- [ ] Una línea explicando el propósito del módulo
- [ ] Contexto de fondo necesario y enlaces
- [ ] Enlaces a fuentes informativas de términos potencialmente desconocidos
- [ ] Ejemplo de uso *ejecutable* y claro
- [ ] Instrucciones de instalación
- [ ] Documentación extensa de la API / librería
- [ ] Realización de [tunelización cognitiva](https://github.com/noffle/art-of-readme#cognitive-funneling)
- [ ] Mencionar excepciones y limitaciones por adelantado
- [ ] No usar imágenes para mostrar información crítica
- [ ] Licencia
